-Mapejat de tecles-

   ↑
 ←   → Utilitza les fletxes del teclat per moure't en el joc.
   ↓

Prem la tecla E per obrir portes.
Prem la tecla F per atacar/destruir objectes destructibles (barrils).
Passa per sobre dels objectes per agafar-los (claus, vides, pocions).


Descripció del joc: L'aventura es desenvolupa en el castell del malvat Ratking,
hauràs de ser un ninja molt àgil i sigil·lós per acabar amb ell.


Com es juga:
El joc consta de 2 pantalles, en la primera el jugador haurà d'agafar la clau que li permeti
passar a la segona pantalla per lluitar amb el boss i podrà agafar la poció de força 
per fer el doble de dany contra el boss. Sense poció de força fan falta 2 tocs 
i amb poció, d'un toc es mor. El jugador comença amb una vida, però en la segona
pantalla, tindrà la possibilitat de tenir una més. Ja en la segona pantalla, per aconseguir una 
vida extra haurà de destruir el barril. Per atacar has d'estar al costat de Ratking i prémer
la tecla 'F'. Si el toques/creues en Ratking et treurà una vida. 
   