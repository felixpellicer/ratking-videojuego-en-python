## Bienvenido al repositorio de RatKing! 👑🐭 (en desarrollo)
<b>Instalación:</b><br>Deberás tener instalado Python y la libreria Pygame, una vez hayas clonado el proyecto realiza la instalación:

<b>En Linux:</b><br><code>*apt-get update*</code><br><code>*apt-get install python3*</code><br><code>*apt-get install python3-pygame*</code><br>

❗Importante, deberás tener los controladores de tu GPU correctamente instalados.<br>

<b>En Windows:</b><br>Descarga el instalador ejecutable de Python [Aquí](https://www.python.org/downloads/ "Aquí") y realiza la instalación. Posteriormente, abre una terminal de CMD en modo administrador y ejecuta el siguiente comando:<br>
<code>*python3 -m pip install -U pygame --user*</code><br><br>
⚠️ Aviso. Si no te funciona el anterior comando y no has podido instalar Pygame, prueba con este <a href="https://www.youtube.com/watch?v=Txb5PPDX_70">tutorial en YouTube.</a>

Listo ya tienes lo necesario para poder jugar a RatKing! ✅<br>

<b>Como se juega?</b><br>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;⬆️<br>
⬅️⬇️➡️&nbsp;&nbsp;&nbsp;Usa las flechas del teclado para moverte en el juego.<br><br>
-Pulsa la tecla E para abrir puertas.<br>-Pulsa la tecla F para atacar/destruir objetos destruibles (barriles).<br>-Pasa por encima de los objetos para agarrarlos (llaves, vidas, pociones).<br>

**Descripción del juego:**&nbsp;&nbsp;*La aventura se desarrolla en el castillo del malvado Ratking, tendrás que ser un ninja muy ágil y sigiloso para acabar con él.*


<b>Instrucciones:</b><br>El juego consta de 2 pantallas, en la primera el jugador tendrá que coger la llave que le permita
pasar a la segunda pantalla para luchar con el boss y podrá coger la poción de fuerza para realizar el doble de daño contra el boss. Sin poción de fuerza hacen falta 2 toques y con poción, de un toque muere. El jugador comienza con una vida, pero en la segunda
pantalla, tendrá la posibilidad de tener una más. Ya en la segunda pantalla, para conseguir una vida extra tendrá que destruir el barril. Para atacar debes estar junto a Ratking y pulsar la tecla 'F'. Si lo tocas/cruzas en Ratking te quitará una vida.
<br>
<p>This work is licensed under <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International</a></p>

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/Cc-by-nc-sa_icon.svg/2560px-Cc-by-nc-sa_icon.svg.png" width="88" height="31">