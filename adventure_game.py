import sys, pygame, time
pygame.font.init()
pygame.init()
size = width, height = 960, 768
#mapas
level=level1_map=[
    [1,1,1,1,1,1,1,1,1,1,1,13,1,1,1],
    [1,0,1,1,1,1,1,1,1,1,1,1,1,1,1],
    [1,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
    [1,1,1,1,1,1,1,1,1,1,1,1,1,0,1],
    [1,1,1,1,3,0,0,0,0,0,1,0,1,0,1],
    [1,1,1,1,0,0,0,0,0,0,1,0,1,0,1],
    [1,0,0,1,0,0,0,0,0,0,0,0,1,0,2],
    [1,0,0,1,0,0,0,0,0,5,1,0,1,0,1],
    [1,0,0,1,1,0,0,1,1,1,1,0,1,0,1],
    [1,0,0,0,0,0,0,0,0,0,0,0,1,0,1],
    [1,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
    [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
]
level2_map=[
    [1,1,1,1,1,1,1,1,1,1,1,13,1,11,1],
    [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
    [1,1,0,0,0,0,0,0,0,0,0,0,0,1,1],
    [1,1,0,0,1,0,0,1,0,0,0,1,0,1,1],
    [1,1,0,0,0,0,0,0,0,0,0,0,0,1,1],
    [1,1,0,0,0,0,0,0,0,0,0,0,0,1,1],
    [0,0,0,0,1,0,0,16,0,1,0,1,0,1,1],
    [1,1,0,0,0,0,0,0,0,0,0,0,0,1,1],
    [1,1,0,0,0,0,1,0,0,0,1,0,0,1,1],
    [1,1,0,0,0,0,0,0,0,0,0,0,8,1,1],
    [1,1,1,1,1,1,1,1,1,1,1,1,9,1,1],
    [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
]
#imagenes cargadas y textos renderizados
screen = pygame.display.set_mode(size)
wall = pygame.image.load("wall.png")
background = pygame.image.load("floor.png")
run=pygame.image.load("run.png")
lockdoor=pygame.image.load("lockdoor.png")
key=pygame.image.load("key.png")
keyinventory=pygame.image.load("keyinventory.png")
stairs=pygame.image.load("stairs.png")
potion=pygame.image.load("potion.png")
strenght=pygame.image.load("strenght.png")
barrel=pygame.image.load("barrel.png")
heart=pygame.image.load("heart.png")
ninjalives1=pygame.image.load("ninjalives1.png")
ninjalives2=pygame.image.load("ninjalives2.png")
ninjalives3=pygame.image.load("ninjalives3.png")
ratkinglives1=pygame.image.load("ratkinglives1.png")
ratkinglives2=pygame.image.load("ratkinglives2.png")
ratkinglives3=pygame.image.load("ratkinglives3.png")
ratking=pygame.image.load("ratking.png")
myfont = pygame.font.SysFont('orbitron', 60)
textsurface_win = myfont.render('You Win!', False, (139, 255, 69))
textsurface_lose = myfont.render('You Died!', False, (243, 49, 49))
n_rows=int(768/64)
n_cols=int(960/64)
#variables de coordenadas (x,y), vidas, victoria, derrota
x=1
y=1
strenghtplus=False
ninjalives=1
ratkinglives=2
finish_win=False
finish_lose=False
active=True
#bucle principal d'ejecución del juego
while active:
    for row in range(0, len(level)):
        for col in range(0, len(level[0])):
            if level[row][col]==0:
                square=background.get_rect().move(col*64, row*64)
                screen.blit(background, square)
            elif level[row][col]==1:
                square=wall.get_rect().move(col*64, row*64)
                screen.blit(wall, square)
            elif level[row][col]==2:
                square=lockdoor.get_rect().move(col*64, row*64)
                screen.blit(lockdoor, square)
            elif level[row][col]==3:
                square=key.get_rect().move(col*64, row*64)
                screen.blit(key, square)
            elif level[row][col]==4:
                square=stairs.get_rect().move(col*64, row*64)
                screen.blit(stairs,square)
            elif level[row][col]==5:
                square=potion.get_rect().move(col*64, row*64)
                screen.blit(potion,square)
            elif level[row][col]==6:
                square=keyinventory.get_rect().move(col*64, row*64)
                screen.blit(keyinventory,square)
            elif level[row][col]==7:
                square=strenght.get_rect().move(col*64, row*64)
                screen.blit(strenght,square)
            elif level[row][col]==8:
                square=barrel.get_rect().move(col*64, row*64)
                screen.blit(barrel, square)
            elif level[row][col]==9:
                square=heart.get_rect().move(col*64, row*64)
                screen.blit(heart, square)
            elif level[row][col]==10:
                square=ratkinglives1.get_rect().move(col*64, row*64)
                screen.blit(ratkinglives1, square)
            elif level[row][col]==11:
                square=ratkinglives2.get_rect().move(col*64, row*64)
                screen.blit(ratkinglives2, square)
            elif level[row][col]==12:
                square=ratkinglives3.get_rect().move(col*64, row*64)
                screen.blit(ratkinglives3, square)
            elif level[row][col]==13:
                square=ninjalives1.get_rect().move(col*64, row*64)
                screen.blit(ninjalives1, square)
            elif level[row][col]==14:
                square=ninjalives2.get_rect().move(col*64, row*64)
                screen.blit(ninjalives2, square)
            elif level[row][col]==15:
                square=ninjalives3.get_rect().move(col*64, row*64)
                screen.blit(ninjalives3, square)
            elif level[row][col]==16:
                square=ratking.get_rect().move(col*64, row*64)
                screen.blit(ratking, square)
    run_rect=run.get_rect().move(x*64,y*64)
    screen.blit(run,run_rect)
    #condiciones de control para las vidas y terminar el juego
    if finish_win==True:
        time.sleep(3)
        active=False
    if finish_lose==True:
        time.sleep(3)
        active=False
    if ninjalives==1:
        level[0][11]=13
    if ninjalives==0:
        level[0][11]=15
        pygame.display.update()
        screen.blit(textsurface_lose,(350,350))
        finish_lose=True
    if ratkinglives ==1:
        level[0][13]=10
    if ratkinglives ==0:
        level[0][13]=12
        pygame.display.update()
        screen.blit(textsurface_win,(350,350))
        finish_win=True
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            active=False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RIGHT:
                if level==level1_map:
                    if level[y][x+1] == 0 or level[y][x+1]==4:
                        x=x+1
                    elif level[y][x+1]==5:
                        level[y][x+1]=0
                        level[0][1]=7
                        strenghtplus=True
                else:
                    if level[y][x+1] == 0:
                        x=x+1
                    elif level[y][x+1]==16:
                        ninjalives=ninjalives-1
            if event.key == pygame.K_LEFT:
                if level==level1_map:
                    if level[y][x-1] == 0:
                        x=x-1
                    elif level[y][x-1]==3:
                        level[y][x-1]=0
                        level[0][0]=6
                else:
                    if level[y][x-1] == 0:
                        x=x-1
                    elif level[y][x-1]==16:
                        ninjalives=ninjalives-1
            if event.key == pygame.K_UP:
                if level==level1_map:
                    if level[y-1][x] == 0:
                        y=y-1
                    elif level[y-1][x]==3:
                        level[y-1][x]=0
                        level[0][0]=6
                else:
                    if level[y-1][x] == 0:
                        y=y-1
                    elif level[y-1][x]==16:
                        ninjalives=ninjalives-1
            if event.key == pygame.K_DOWN:
                if level==level1_map:
                    if level[y+1][x] == 0:
                        y=y+1
                    elif level[y+1][x]==5:
                        level[y+1][x]=0
                        level[0][1]=7
                        strenghtplus=True
                else:
                    if level[y+1][x] == 0:
                        y=y+1
                    elif level[y+1][x] == 9:
                        level[y+1][x]=0
                        level[0][11]=14
                        ninjalives=ninjalives+1
                    elif level[y+1][x]==16:
                        ninjalives=ninjalives-1
            if event.key == pygame.K_e:
                if level==level1_map:
                    if level[y][x+1]==2 and level[0][0]==6:
                        level[6][14]=4
                        level[0][0]=1
            if event.key == pygame.K_f:
                if level==level2_map:
                    if level[y][x+1] == 8:
                        level[9][12]=0
                    elif level[y+1][x]==8:
                        level[9][12]=0
                    elif level[y][x+1]==level[6][7] or level[y][x-1]==level[6][7] or level[y+1][x]==level[6][7] or level[y-1][x]==level[6][7]:
                        if strenghtplus:
                            ratkinglives=ratkinglives-2
                        else:
                            ratkinglives=ratkinglives-1
    pygame.display.flip()
    #cambio de mapa, inicio de nuevas coordenadas en el mapa2
    if level1_map[6][14]==4 and level1_map[y][x]==4:
        level=level2_map
        x=1
        y=6
        if strenghtplus:
            level[0][1]=7
pygame.quit()